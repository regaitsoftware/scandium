﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Scandium.Classes;
using Scandium.Enums;

namespace ScandiumTests
{
    [TestClass]
    public class JointTests
    {
        /// <summary>
        ///     A Joint constructor should create a Joint with the correct specified values.
        /// </summary>
        [TestMethod]
        public void JointTest()
        {
            var testJoint = new Joint(1, 2, 3, JointType.LeftHip);
            Assert.AreEqual(1, testJoint.X);
            Assert.AreEqual(2, testJoint.Y);
            Assert.AreEqual(3, testJoint.Z);
            Assert.AreEqual(JointType.LeftHip, testJoint.JointType);
        }

        /// <summary>
        ///     Joints with a Y difference of 0 should return 45 degrees.
        /// </summary>
        [TestMethod]
        public void GetAngleTest45Degrees()
        {
            var j1 = new Joint(0, 0, 0, JointType.LeftHip);
            var j2 = new Joint(0, 0, 0, JointType.LeftKnee);
            Assert.AreEqual(45, Joint.GetAngle(j1, j2));
        }

        /// <summary>
        ///     Joints with a Y difference of 0 and the hip joint is closer to the cam, return -45 degrees.
        /// </summary>
        [TestMethod]
        public void GetAngleTestNeg45Degrees()
        {
            var j1 = new Joint(0, 0, 0, JointType.LeftHip);
            var j2 = new Joint(0, 0, 1, JointType.LeftKnee);
            Assert.AreEqual(-45, Joint.GetAngle(j1, j2));
        }

        /// <summary>
        ///     Testing out the regular case of two joints not in a 45 degree angle. -- Arc Cos of delta Y / (Square delta x^2 +
        ///     delta y^2 + delta z^2)
        /// </summary>
        [TestMethod]
        public void GetAngleTestStandardCase()
        {
            var j1 = new Joint(2, 2, 0, JointType.LeftHip);
            var j2 = new Joint(2, 1, 2, JointType.LeftKnee);
            Assert.AreEqual(-63, Math.Round(Joint.GetAngle(j1, j2)));
        }

        /// <summary>
        ///     Make sure ToString method works as expected
        /// </summary>
        [TestMethod]
        public void ToStringTest()
        {
            var j1 = new Joint(1, 2, 3, JointType.LeftHip);
            var expected = $"({j1.X},{j1.Y},{j1.Z}), {j1.JointType}";
            Assert.AreEqual(expected, j1.ToString());
        }
    }
}