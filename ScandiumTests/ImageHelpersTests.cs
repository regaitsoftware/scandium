﻿using System.Windows.Media.Animation;
using Microsoft.Kinect;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ScandiumTests
{
    public interface IMockColorFrame
    {
        FrameDescription FrameDescription { get; }
        void CopyRawFrameDataToArray(byte[] frameData);
        void CopyConvertedFrameDataToArray(byte[] frameData, ColorImageFormat colorFormat);
        ColorImageFormat RawColorImageFormat { get; }
        int BitsPerPixel { get; }
    }

    [TestClass()]
    public class ImageHelpersTests
    {
        [TestMethod()]
        public void ToBitmapSourceTestColor()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToBitmapSourceTestDepth()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToBitmapSourceTestInfrared()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToCvImageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToUMatTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToBitmapSourceTest3()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void ToBitmapTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void GetDepthCoordinatesTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SmoothImageTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void FindBlobsTest()
        {
            Assert.Fail();
        }
    }
}