﻿using Microsoft.Kinect;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScandiumTests
{
    [TestClass()]
    public class KinectSensorTests
    {
        [TestMethod()]
        public void KinectSensorConstructor_ShouldSetCoordinateMapperAndFrameReader()
        {
            var expectedFrameSourceTypes = FrameSourceTypes.Color | FrameSourceTypes.Depth;
            var sensor = KinectSensor.GetDefault();
            var kinectSensor = new Scandium.Classes.KinectSensor(sensor, expectedFrameSourceTypes);
            Assert.IsNotNull(sensor);
            Assert.AreEqual(kinectSensor.MultiSourceFrameReader.FrameSourceTypes, expectedFrameSourceTypes);
            Assert.AreEqual(sensor.CoordinateMapper, kinectSensor.CoordinateMapper);
        }

        [TestMethod()]
        public void Close_ShouldClose_KinectSensor()
        {
            var sensor = KinectSensor.GetDefault();
            var kinectSensor = new Scandium.Classes.KinectSensor(sensor, FrameSourceTypes.Color | FrameSourceTypes.Depth);
            kinectSensor.Close();
            Assert.AreEqual(sensor.IsOpen, false);
        }

        [TestMethod()]
        public void OpenTest()
        {
            var sensor = KinectSensor.GetDefault();
            var kinectSensor = new Scandium.Classes.KinectSensor(sensor, FrameSourceTypes.Color | FrameSourceTypes.Depth);
            kinectSensor.Open();
            Assert.AreEqual(sensor.IsOpen, true);
        }
    }
}