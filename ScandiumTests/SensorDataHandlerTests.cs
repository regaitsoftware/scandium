﻿using System;
using System.Drawing;
using System.Collections.Generic;
using Emgu.CV;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scandium.Classes;
using Scandium.Enums;
using Scandium.EventArgs;
using Scandium.Interfaces;

namespace ScandiumTests
{
    [TestClass]
    public class SensorDataHandlerTests
    {
        [TestMethod]
        public void SensorDataHandlerConstructor_NoOpenCL_ShouldNotSetUseOpenCL()
        {
            var mockInvoker = new Mock<ICvInvoker>();
            mockInvoker.Setup(x => x.HaveOpenCL).Returns(false);
            var sensorDataHandler = new SensorDataHandler(mockInvoker.Object);
            Assert.IsInstanceOfType(sensorDataHandler.MessageBox, typeof(MessageBox));
            Assert.AreEqual(SessionState.Standby, sensorDataHandler.CurrentSessionState);
            mockInvoker.VerifyGet(x => x.HaveOpenCL, Times.Once);
            mockInvoker.Verify(x => x.UseOpenCL, Times.Never);
        }

        [TestMethod]
        public void SensorDataHandlerConstructor_YesOpenCL_ShouldSetUseOpenCL()
        {
            var mockInvoker = new Mock<ICvInvoker>();
            mockInvoker.Setup(x => x.HaveOpenCL).Returns(true);
            var sensorDataHandler = new SensorDataHandler(mockInvoker.Object);
            Assert.IsInstanceOfType(sensorDataHandler.MessageBox, typeof(MessageBox));
            Assert.AreEqual(SessionState.Standby, sensorDataHandler.CurrentSessionState);
            mockInvoker.VerifyGet(x => x.HaveOpenCL, Times.Once);
            mockInvoker.VerifySet(x => x.UseOpenCL = true, Times.Once);
        }

        [TestMethod]
        public void InitArduinoSensors_ExistingPort_GetsLoadCellPort()
        {
            var portNames = "foo";
            var mockPortFetcher = new Mock<IPortFetcher>();
            var mockMessageBox = new Mock<IMessageBox>();
            mockPortFetcher.Setup(x => x.GetLoadCellPort()).Returns(new SerialPort(portNames));
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.PortFetcher = mockPortFetcher.Object;
            sensorDataHandler.MessageBox = mockMessageBox.Object;
            sensorDataHandler.InitArduinoSensors();
            mockMessageBox.Verify(x => x.Show(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }

        [TestMethod]
        public void InitArduinoSensors_NoPorts_ShowsErrorMessage()
        {
            var expectedStrings = new[]
            {
                "Weight Sensor Error!",
                "No weight sensors found. Weight data will not be recorded. Please check connections on unit."
            };
            var mockMessageBox = new Mock<IMessageBox>();
            var sensorDataHandler = MakeSensorDataHandler();
            var mockPortFetcher = new Mock<IPortFetcher>();
            mockPortFetcher.Setup(x => x.GetLoadCellPort()).Throws(null);
            sensorDataHandler.MessageBox = mockMessageBox.Object;
            sensorDataHandler.PortFetcher = mockPortFetcher.Object;
            sensorDataHandler.InitArduinoSensors();
            mockMessageBox.Verify(x => x.Show(expectedStrings[0], expectedStrings[1]), Times.Once());
        }

        [TestMethod]
        public void InitKinect_ShouldMakeScandiumKinectSensor()
        {
            var x = MakeSensorDataHandler();
            x.InitKinect();
            Assert.IsNotNull(x.ScandiumKinectSensor);
        }

        [TestMethod]
        public void Dispose_ShouldCloseSensors()
        {
            var mockKinect = new Mock<IKinectSensor>();
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.ScandiumKinectSensor = mockKinect.Object;
            sensorDataHandler.Dispose();
            mockKinect.Verify(x => x.Close(), Times.Once);
        }

        [TestMethod]
        public void OpenSensors_ShouldOpenAllSensors()
        {
            var mockKinect = new Mock<IKinectSensor>();
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.ScandiumKinectSensor = mockKinect.Object;
            sensorDataHandler.OpenSensors();
            mockKinect.Verify(x => x.Open(), Times.Once);
        }

        [TestMethod]
        public void CloseSensors_ShouldClosesAllMemberSensorsAndStopsStopwatch()
        {
            var mockKinect = new Mock<IKinectSensor>();
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.ScandiumKinectSensor = mockKinect.Object;
            sensorDataHandler.CloseSensors();
            mockKinect.Verify(x => x.Close(), Times.Once);
        }

        [TestMethod]
        public void QueryLoadCells_Reads5Values()
        {
            var mockLoadCellPort = new Mock<ISerialPort>();
            mockLoadCellPort.Setup(x => x.ReadLine()).Returns("10|11|12|13|13");
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.LoadCellPort = mockLoadCellPort.Object;
            var actual = sensorDataHandler.QueryLoadCells();
            var expected = new[] {11.0f, 12.0f, 8.0f, 8.0f};
            CollectionAssert.AreEqual(expected, actual);
            mockLoadCellPort.Verify(x =>x.DiscardInBuffer(), Times.Once());
            mockLoadCellPort.Verify(x => x.ReadLine(), Times.Once);
        }

        [TestMethod]
        public void QueryLoadCells_Reads4Values()
        {
            var mockLoadCellPort = new Mock<ISerialPort>();
            mockLoadCellPort.Setup(x => x.ReadLine()).Returns("10|11|12|13|");
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.LoadCellPort = mockLoadCellPort.Object;
            var actual = sensorDataHandler.QueryLoadCells();
            var expected = new[] {10.0f, 10.0f, 10.0f, 10.0f};
            CollectionAssert.AreEqual(expected, actual);
            mockLoadCellPort.Verify(x =>x.DiscardInBuffer(), Times.Once());
            mockLoadCellPort.Verify(x => x.ReadLine(), Times.Once);
        }

        [TestMethod]
        public void QueryLoadCells_ReadLine_ThrowsException()
        {
            var mockLoadCellPort = new Mock<ISerialPort>();
            mockLoadCellPort.Setup(x => x.ReadLine()).Throws<Exception>();
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.LoadCellPort = mockLoadCellPort.Object;
            var actual = sensorDataHandler.QueryLoadCells();
            var expected = new[] { 10.0f, 10.0f, 10.0f, 10.0f };
            CollectionAssert.AreEqual(expected, actual);
            mockLoadCellPort.Verify(x => x.DiscardInBuffer(), Times.Once());
            mockLoadCellPort.Verify(x => x.ReadLine(), Times.Once);
        }

        [TestMethod]
        public void QueryLoadCells_NullPort_ReturnsDefault10s()
        {
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.LoadCellPort = null;
            var actual = sensorDataHandler.QueryLoadCells();
            var expected = new[] { 10.0f, 10.0f, 10.0f, 10.0f };
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void QueryLoadCells_OpenThrowsExceptionAsExpected()
        {
            var mockLoadCellPort = new Mock<ISerialPort>();
            mockLoadCellPort.Setup(x => x.ReadLine()).Throws<Exception>();
            mockLoadCellPort.Setup(x => x.Open()).Throws<Exception>();
            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.LoadCellPort = mockLoadCellPort.Object;
            var actual = sensorDataHandler.QueryLoadCells();
            var expected = new[] { 10.0f, 10.0f, 10.0f, 10.0f };
            CollectionAssert.AreEqual(expected, actual);
            mockLoadCellPort.Verify(x => x.DiscardInBuffer(), Times.Once());
            mockLoadCellPort.Verify(x => x.ReadLine(), Times.Once);
        }

        [TestMethod]
        public void QueryJointInformation_NotEnoughBlobs_ShouldReturnsAnglesOfZeroAndEmptyDictionary()
        {
            var mockBlobs = new List<Blob>();
            var mockBlobDetector = new Mock<IBlobDetector>();
            mockBlobDetector.Setup(x => x.FindBlobs(It.IsAny<IInfraredFrame>())).
                Returns(new Tuple<List<Blob>, IImage>(mockBlobs, new UMat()));

            var mockDepthFrame = new Mock<IDepthFrame>();
            var fakeDepthCoordinate = new ushort[0];
            mockDepthFrame.Setup(x => x.GetDepthCoordinates(It.IsAny<IKinectSensor>()))
                .Returns(fakeDepthCoordinate);

            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.BlobDetector = mockBlobDetector.Object;
            var jointInfo = sensorDataHandler.QueryJointInformation(new Mock<IInfraredFrame>().Object, mockDepthFrame.Object);

            CollectionAssert.Contains(jointInfo.Item1, 0.0d);
            Assert.IsTrue(jointInfo.Item2.Count == 0);
        }

        [TestMethod]
        public void QueryJointInformation_EnoughBlobs_ShouldReturnsCorrectTupleInfo()
        {
            var mockBlobs = new List<Blob>();
            var fakeDepthCoordinate = GenerateFakeDepthCoordinates();
            for (int i = 0; i <= 5; i++)
            {
                var tmpBlob = new Blob { Centroid = new PointF(i, i) };
                mockBlobs.Add(tmpBlob);
            }

            var mockBlobDetector = new Mock<IBlobDetector>();
            mockBlobDetector.Setup(x => x.FindBlobs(It.IsAny<IInfraredFrame>())).
                Returns(new Tuple<List<Blob>, IImage>(mockBlobs, new UMat()));

            var mockDepthFrame = new Mock<IDepthFrame>();
            mockDepthFrame.Setup(x => x.GetDepthCoordinates(It.IsAny<IKinectSensor>()))
                .Returns(fakeDepthCoordinate.ToArray);

            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.BlobDetector = mockBlobDetector.Object;
            var jointInfo = sensorDataHandler.QueryJointInformation(new Mock<IInfraredFrame>().Object, mockDepthFrame.Object);
            
            CollectionAssert.AreEqual(jointInfo.Item1, new double[] {135, 135, 135, 135});
            Assert.IsTrue(jointInfo.Item2.Count == 6);
        }

        private static List<ushort> GenerateFakeDepthCoordinates()
        {
            var fakeDepthCoordinate = new List<ushort>();
            for (int i = 0; i < 100; i++)
            {
                fakeDepthCoordinate.Insert(i, Constants.MinimumDepthAllowed + 1);
            }
            return fakeDepthCoordinate;
        }

        [TestMethod]
        public void SetSessionState_ShouldOpenSensors_IfClosedBefore()
        {
            var sensorDataHandler = MakeSensorDataHandler();
            var mockKinect = new Mock<IKinectSensor>();
            sensorDataHandler.ScandiumKinectSensor = mockKinect.Object;
            sensorDataHandler.CurrentSessionState = SessionState.DebugOn;
            mockKinect.Verify(x => x.Open(), Times.Once);
        }

        [TestMethod]
        public void SetSessionState_ShouldCloseSensors_IfOpenBefore()
        {
            var sensorDataHandler = MakeSensorDataHandler();
            var mockKinect = new Mock<IKinectSensor>();
            sensorDataHandler.ScandiumKinectSensor = mockKinect.Object;
            sensorDataHandler.CurrentSessionState = SessionState.DebugOn;
            sensorDataHandler.CurrentSessionState = SessionState.Standby;
            mockKinect.Verify(x => x.Close(), Times.Once);
        }

        [TestMethod]
        public void QuerySensors_ShouldFireEvent_When6JointsAreDetected()
        {
            var receivedEvents = new List<EventArgs>();
            var mockBlobs = new List<Blob>();
            var fakeDepthCoordinate = GenerateFakeDepthCoordinates();
            for (int i = 0; i <= 5; i++)
            {
                var tmpBlob = new Blob { Centroid = new PointF(i, i) };
                mockBlobs.Add(tmpBlob);
            }
            var mockBlobDetector = new Mock<IBlobDetector>();
            mockBlobDetector.Setup(x => x.FindBlobs(It.IsAny<IInfraredFrame>())).
                Returns(new Tuple<List<Blob>, IImage>(mockBlobs, new UMat()));

            var mockDepthFrame = new Mock<IDepthFrame>();
            mockDepthFrame.Setup(x => x.GetDepthCoordinates(It.IsAny<IKinectSensor>()))
                .Returns(fakeDepthCoordinate.ToArray);

            var mockColorFrame = new Mock<IInfraredFrame>();

            var sensorDataHandler = MakeSensorDataHandler();
            sensorDataHandler.BlobDetector = mockBlobDetector.Object;
            sensorDataHandler.SensorDataArrived += (sender, e) => receivedEvents.Add(e);
            sensorDataHandler.QuerySensors(mockColorFrame.Object, mockDepthFrame.Object);

            Assert.AreEqual(receivedEvents.Count, 1);
            Assert.IsInstanceOfType(receivedEvents[0], typeof(SensorDataArrivedEventArgs));
        }

        private static SensorDataHandler MakeSensorDataHandler()
        {
            var mockInvoker = new Mock<ICvInvoker>();
            mockInvoker.Setup(x => x.HaveOpenCL).Returns(true);
            return new SensorDataHandler(mockInvoker.Object);
        }
    }
}