﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Common.Auxiliary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScandiumTests
{
    [TestClass]
    public class GraphTests
    {
        /// <summary>
        ///     The ScandiumChartPlotter constructor should take a ChartPlotter.
        /// </summary>
        [TestMethod]
        public void GraphTest()
        {
            var expectedPlotter = new Microsoft.Research.DynamicDataDisplay.ChartPlotter();
            var graph = new Scandium.Classes.ChartPlotter(expectedPlotter);
            Assert.AreEqual(expectedPlotter, graph.Plotter);
        }

        /// <summary>
        ///     Clearing the ScandiumChartPlotter should remove all LineGraphs under the ChartPlotter.
        /// </summary>
        [TestMethod]
        public void ClearTest()
        {
            var graph = new Scandium.Classes.ChartPlotter(new Microsoft.Research.DynamicDataDisplay.ChartPlotter());
            graph.Plotter.AddChild(new LineGraph());
            Assert.AreEqual(1, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
            graph.Clear();
            Assert.AreEqual(0, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
        }

        /// <summary>
        ///     CreateLineGraph should fall back on the previous DataSources.
        /// </summary>
        [TestMethod]
        public void CreateLineGraphNoParamsTest()
        {
            var graph = new Scandium.Classes.ChartPlotter(new Microsoft.Research.DynamicDataDisplay.ChartPlotter());
            var testList = new List<double> {1, 2, 3};
            Assert.AreEqual(0, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
            graph.DataSourceX = testList;
            graph.DataSourceY = testList;
            graph.CreateLineGraph();
            Assert.AreEqual(1, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
        }

        /// <summary>
        ///     CreateLineGraph should accept a tuple.
        /// </summary>
        [TestMethod]
        public void CreateLineGraphTupleTest()
        {
            var testList = new List<double> {1, 2, 3};
            var tuple = new Tuple<IEnumerable<double>, IEnumerable<double>>(testList, testList);
            var graph = new Scandium.Classes.ChartPlotter(new Microsoft.Research.DynamicDataDisplay.ChartPlotter());
            Assert.AreEqual(0, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
            graph.CreateLineGraph(tuple);
            Assert.AreEqual(graph.DataSourceX, tuple.Item1);
            Assert.AreEqual(graph.DataSourceY, tuple.Item2);
            Assert.AreEqual(1, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
        }

        /// <summary>
        ///     CreateLineGraph should accepts 2 lists.
        /// </summary>
        [TestMethod]
        public void CreateLineGraphListsTest()
        {
            var testList = new List<double> {1, 2, 3};
            var graph = new Scandium.Classes.ChartPlotter(new Microsoft.Research.DynamicDataDisplay.ChartPlotter());
            Assert.AreEqual(0, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
            graph.CreateLineGraph(testList, testList);
            Assert.AreEqual(graph.DataSourceX, testList);
            Assert.AreEqual(graph.DataSourceY, testList);
            Assert.AreEqual(1, graph.Plotter.Children.Count(x => x.GetType() == typeof (LineGraph)));
        }
    }
}