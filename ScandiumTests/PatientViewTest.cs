﻿using System.Windows;
using Emgu.CV;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Scandium.Enums;
using Scandium.EventArgs;
using Scandium.Interfaces;
using PatientView = Scandium.Windows.PatientView;

namespace ScandiumTests
{
    [TestClass]
    public class PatientViewTest
    {
        [TestMethod]
        public void PatientView_ShouldInitializeWithResolutionAndInitAllSensors()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            var window = new PatientView(mockSensorDataHandler.Object);
            Assert.AreEqual(600, window.Height);
            Assert.AreEqual(1024, window.Width);
            mockSensorDataHandler.Verify(x => x.InitArduinoSensors(), Times.Once);
            mockSensorDataHandler.Verify(x => x.InitKinect(), Times.Once);
        }

        [TestMethod]
        public void PatientView_ShouldDisposeDataHandler_OnClose()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            var window = new PatientView(mockSensorDataHandler.Object);
            window.Close();
            mockSensorDataHandler.Verify(x => x.Dispose(), Times.Once);

        }

        private static VideoFrameArrivedEventArgs MakeVideoFrameArrivedEventArgs()
        {
            var mockColorFrame = new Mock<IInfraredFrame>();
            mockColorFrame.Setup(x => x.ToUMat()).Returns(new UMat());
            return new VideoFrameArrivedEventArgs(mockColorFrame.Object);
        }

        [TestMethod]
        public void PatientView_OnCalibrateButtonClicked_ShouldChangeCurrentStateToCalibrationOnFirstClick()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            mockSensorDataHandler.Setup(x => x.CurrentSessionState).Returns(SessionState.Standby);
            var fakeWindow = new PatientView(mockSensorDataHandler.Object);
            fakeWindow.OnCalibrateButtonClicked(new object(), new RoutedEventArgs());
            mockSensorDataHandler.VerifySet(x => x.CurrentSessionState=SessionState.DebugOn, Times.Once);
        }

        [TestMethod]
        public void PatientView_OnCalibrateButtonClicked_ShouldChangeCurrentStateToStandbyOnSecondClick()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            mockSensorDataHandler.Setup(x => x.CurrentSessionState).Returns(SessionState.DebugOn);
            var fakeWindow = new PatientView(mockSensorDataHandler.Object);
            fakeWindow.OnCalibrateButtonClicked(new object(), new RoutedEventArgs());
            mockSensorDataHandler.VerifySet(x => x.CurrentSessionState=SessionState.Standby, Times.Once);
        }

        [TestMethod]
        public void PatientView_OnStartSessionButtonClicked_ShouldChangeCurrentStateToSessionOnOnFirstClick()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            mockSensorDataHandler.Setup(x => x.CurrentSessionState).Returns(SessionState.Standby);
            var fakeWindow = new PatientView(mockSensorDataHandler.Object);
            fakeWindow.OnStartSessionButtonClicked(new object(), new RoutedEventArgs());
            mockSensorDataHandler.VerifySet(x => x.CurrentSessionState=SessionState.SessionOn, Times.Once);
        }

        [TestMethod]
        public void PatientView_OnStartSessionButtonClicked_ShouldChangeCurrentStateToStandbyOnSecondClick()
        {
            var mockSensorDataHandler = new Mock<ISensorDataHandler>();
            mockSensorDataHandler.Setup(x => x.CurrentSessionState).Returns(SessionState.SessionOn);
            var fakeWindow = new PatientView(mockSensorDataHandler.Object);
            fakeWindow.OnStartSessionButtonClicked(new object(), new RoutedEventArgs());
            mockSensorDataHandler.VerifySet(x => x.CurrentSessionState=SessionState.Standby, Times.Once);
        }
    }
}