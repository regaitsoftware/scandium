Treat .xef file as connected Kinect
------------------------------------

1)  Select a .xef file in the KinectTestData and open it (There is one in the Google Drive Shared Folder)
    Make sure .xef file is valid IR and Depth only recordings.
2)  Go to the �Play� Tab in Kinect Studio
3)  Make sure all streams are visible (for the top level �streams� sometimes you have to click the �eye� to hide all and then click each individual stream�s �eye� to get all of them visible)
4)  Increase the loop count to whatever length you want, this will keep it playing while you test your app
5)  Click the connection symbol to �connect� the virtual Kinect service
6)  Click play to start the data stream
7)  Start
Please check the HowToRun.docx file to see pictures for more information.


Run Application
---------------

1)  Make sure to setup and run scandium-website, without it running you will not be able to login.
2)  Go to Tools -> Nuget Package Manager -> Package Manager Console. This should
    prompt for restoring packages, do this otherwise blob detection and other features will not work.
3)  Install Kinect Studio.
4)  Open sample .xef file in Kinect Studio.
5)  Attach as Kinect feed as described above.
6)  The username is hardcoded to "testpatient@example.com" make sure such a user exists on scandium-website.
7)  Login by entering the user's 6 digit PIN.
8)  Make sure the local api endpoint referenced in LoginView.xaml.cs and PatientView.xaml.cs are referring to the right port (should be 44306)
9)  Run app.
10) Click debug button to see camera feed that is being processed to confirm the Kinect feed is being received.
11) Click start session button to start recording the data. When you finish the session an api call will be made to scandium-website.


Notes
-----
Unfortunately there has been no developer friendly way to spoof com ports for the arduino,
so testing the loadcells and wheel encoder requires the developer to be on the walker.

Login as "testpractitioner@example.com" to see the session data on scandium-website.
