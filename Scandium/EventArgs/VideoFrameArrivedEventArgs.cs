﻿using Scandium.Interfaces;

namespace Scandium.EventArgs
{
    internal class VideoFrameArrivedEventArgs : System.EventArgs
    {
        public VideoFrameArrivedEventArgs(IInfraredFrame frame)
        {
            VideoFrame = frame;
        }

        public IInfraredFrame VideoFrame { get; }
    }
}