﻿using System;
using System.Collections.Generic;
using Scandium.Enums;
using Scandium.Classes;

namespace Scandium.EventArgs
{
    internal class SensorDataArrivedEventArgs : System.EventArgs
    {
        public double[] Angles;
        public DateTime TimeOfEvent;

        public SensorDataArrivedEventArgs(float[] loadCells, Dictionary<JointType, Joint> joints, double[] angles,
            DateTime timeOfEvent)
        {
            LoadCells = loadCells;
            Joints = joints;
            Angles = angles;
            TimeOfEvent = timeOfEvent;
        }

        public float[] LoadCells { get; }
        public Dictionary<JointType, Joint> Joints { get; }
    }
}