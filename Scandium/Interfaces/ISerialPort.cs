﻿namespace Scandium.Interfaces
{
    internal interface ISerialPort
    {
        string Name { get; set; }
        void Open();
        void Close();
        void DiscardInBuffer();
        string ReadExisting();
        string ReadLine();
        void Write(byte[] b);
        void Ping();
    }
}