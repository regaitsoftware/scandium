using System;
using System.Collections.Generic;
using System.Windows.Media;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;

namespace Scandium.Interfaces
{
    internal interface IChartPlotter
    {
        Pen Pen { get; set; }
        CirclePointMarker CirclePointMarker { get; set; }
        PenDescription PenDescription { get; set; }
        ChartPlotter Plotter { get; set; }
        IEnumerable<double> DataSourceX { get; set; }
        IEnumerable<double> DataSourceY { get; set; }
        void Clear();
        void CreateLineGraph(IEnumerable<double> dataSourceX, IEnumerable<double> dataSourceY);
        void CreateLineGraph(Tuple<IEnumerable<double>, IEnumerable<double>> tuple);
        void CreateLineGraph();
    }
}