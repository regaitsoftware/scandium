﻿using System.Drawing;

namespace Scandium.Interfaces
{
    internal interface IBlob
    {
        float Area { get; set; }
        PointF Centroid { get; set; }
    }
}