namespace Scandium.Interfaces
{
    public interface ICvInvoker
    {
        bool HaveOpenCL { get; }
        bool UseOpenCL { get; set; }
    }
}