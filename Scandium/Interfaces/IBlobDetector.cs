﻿using System;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;
using Scandium.Classes;

namespace Scandium.Interfaces
{
    internal interface IBlobDetector
    {
        Tuple<List<Blob>, IImage> FindBlobs(IInfraredFrame frame);
    }
}
