﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scandium.Interfaces
{
    public interface IInfraredFrame : IKinectFrame
    {
        void CopyFrameDataToArray(ushort[] frameData);
    }
}
