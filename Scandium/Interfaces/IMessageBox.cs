using System.Windows;

namespace Scandium.Interfaces
{
    public interface IMessageBox
    {
        MessageBoxResult Show(string title, string message);
    }
}