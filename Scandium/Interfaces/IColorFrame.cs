﻿using Microsoft.Kinect;

namespace Scandium.Interfaces
{
    public interface IColorFrame: IKinectFrame
    {
        ColorImageFormat RawColorImageFormat { get; }
        void CopyConvertedFrameDataToArray(byte[] frameData, ColorImageFormat colorFormat);
        void CopyRawFrameDataToArray(byte[] frameData);
    }
}
