﻿using Microsoft.Kinect;

namespace Scandium.Interfaces
{
    public interface IKinectSensor
    {
        CoordinateMapper CoordinateMapper { get; }
        MultiSourceFrameReader MultiSourceFrameReader { get; }
        MultiSourceFrameReader OpenMultiSourceFrameReader(FrameSourceTypes frameSourceTypes);
        void Close();
        void Open();
    }
}