﻿namespace Scandium.Interfaces
{
    public interface IDepthFrame : IKinectFrame
    {
        ushort DepthMinReliableDistance { get; }
        ushort DepthMaxReliableDistance { get; }
        void CopyFrameDataToArray(ushort[] frameData);
        ushort[] GetDepthCoordinates(IKinectSensor scandiumKinectSensor);
    }
}
