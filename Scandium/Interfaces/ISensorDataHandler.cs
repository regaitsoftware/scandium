using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Kinect;
using Scandium.Enums;
using Scandium.EventArgs;
using Joint = Scandium.Classes.Joint;
using JointType = Scandium.Enums.JointType;

namespace Scandium.Interfaces
{
    internal interface ISensorDataHandler : IDisposable
    {
        IKinectSensor ScandiumKinectSensor { get; set; }
        IMessageBox MessageBox { get; set; }
        IPortFetcher PortFetcher { get; set; }
        IBlobDetector BlobDetector { get; set; }
        SessionState CurrentSessionState { get; set; }
        ISerialPort LoadCellPort { get; set; }
        event EventHandler<VideoFrameArrivedEventArgs> VideoFrameArrived;
        event EventHandler<SensorDataArrivedEventArgs> SensorDataArrived;
        void InitArduinoSensors();
        void InitKinect();
        void OpenSensors();
        void CloseSensors();
        void QuerySensors(IInfraredFrame color, IDepthFrame depth);
        float[] QueryLoadCells();
        Tuple<double[], Dictionary<JointType, Joint>> QueryJointInformation(IInfraredFrame color, IDepthFrame depth);
        void KinectFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e);
    }
}