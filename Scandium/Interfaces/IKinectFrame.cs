﻿using System;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Scandium.Interfaces
{
    public interface IKinectFrame : IDisposable
    {
        int Width { get; }
        int Height { get; }
        Image<Bgr, byte> ToCvImage();
        UMat ToUMat();
        BitmapSource ToBitmapSource();
    }
}
