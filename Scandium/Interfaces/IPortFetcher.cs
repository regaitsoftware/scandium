using System.Collections.Generic;

namespace Scandium.Interfaces
{
    internal interface IPortFetcher
    {
        List<ISerialPort> GetAvailablePorts();
        ISerialPort GetLoadCellPort();
        string[] GetPortNames();
    }
}