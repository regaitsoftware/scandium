﻿using System;
using System.IO.Ports;
using Scandium.Interfaces;
using System.Threading;

namespace Scandium.Classes
{
    internal class SerialPort : ISerialPort
    {
        public string Name { get; set; }
        private readonly System.IO.Ports.SerialPort _port;

        public SerialPort(string portName)
        {
            _port = new System.IO.Ports.SerialPort(portName, 9600, Parity.None, 8, StopBits.One)
            {
                DtrEnable = true,
                NewLine = "\n"
            };
            Name = _port.PortName;
        }

        public void Open()
        {
            _port.Open();
        }

        public void Close()
        {
            _port.Close();
        }

        public void DiscardInBuffer()
        {
            _port.DiscardInBuffer();
        }

        public string ReadExisting()
        {
            return _port.ReadExisting();
        }

        public string ReadLine()
        {
            return _port.ReadLine();
        }
        public void Write(byte[] b)
        {
            _port.Write(b, 0,1);
        }
        public void Ping()
        {
            byte[] pingByte= new byte[1];
            pingByte[0] = 3;
            Write(pingByte);
            //Thread.Sleep(10);
        }
    }
}