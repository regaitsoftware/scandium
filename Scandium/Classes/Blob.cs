﻿using System.Drawing;
using Emgu.CV.Cvb;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class Blob : IBlob
    {
        public PointF Centroid { get; set; }
        public float Area { get; set; }
        public Blob() { }
        public Blob(CvBlob blob)
        {
            Centroid = blob.Centroid;
            Area = blob.Area;
        }
    }
}
