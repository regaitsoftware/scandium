﻿using System;
using System.Collections.Generic;
using System.Linq;
using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class BlobDetector : IBlobDetector
    {

        /// <summary>
        ///     Given a ColorFrame, filter image by MaxHSV and MinHSV. Return blobs detected by EMGU CV.
        /// </summary>
        /// <param name="frame">ColorFrame used for blob detection.</param>
        /// <returns>CvBlobs detected from Kinect ColorFrame.</returns>
        public Tuple<List<Blob>, IImage> FindBlobs(IInfraredFrame frame)
        {
            // Filter for red color from sliders. Use UMat for GPU processing.
            using (var detector = new CvBlobDetector())
            {
                var filtered = FilterFrame(frame);

                // Initialize blobs datatype.
                var blobs = new CvBlobs();
                // Detect all blobs, then filter out small or large blobs unlikely to be the blobs we care about.
                detector.Detect(filtered, blobs);
                // ir blobs are now smaller, make sure to only grab blobs generally this size
                blobs.FilterByArea(Constants.MinimumBlobSize, Constants.MaximumBlobSize);
                // don't grab blobs from outside of the general middle-type area
                var filteredBlobs = blobs.Where(b => Between((int)b.Value.Centroid.X, Constants.MinimumBlobXPosition, Constants.MaximumBlobXPosition));

                var sortedBlobs = OrderBlobsForJointConversion(filteredBlobs);
                var drawn = detector.DrawBlobs(filtered, blobs, CvBlobDetector.BlobRenderType.Default, 0.5f);
                return new Tuple<List<Blob>, IImage>(sortedBlobs, drawn);
            }
        }

        private Image<Gray, byte> FilterFrame(IKinectFrame frame)
        {
            using (var hsv = new UMat())
            using (var filtered = new UMat())
            {
                // HSVs are more reliable than Bgr images in blob detection due to higher color contrast.
                CvInvoke.CvtColor(frame.ToUMat(), hsv, ColorConversion.Bgr2Hsv);
                // Prepare HSV for filtering.
                hsv.SmoothImage();
                // Color filtering occurs
                CvInvoke.InRange(hsv, Constants.LowerScalarArray, Constants.UpperScalarArray, filtered);
                filtered.SmoothImage();
                return filtered.ToImage<Gray, byte>();
            }
        }

        private static List<Blob> OrderBlobsForJointConversion(IEnumerable<KeyValuePair<uint, CvBlob>> blobs)
        {
            // Sorting approach to detect all joints
            // Order by X first to get left and right sides of legs.
            var orderedBlobs = blobs.OrderByDescending(x => x.Value.Centroid.X);
            // Sort each half of collection by y to get joints (Higher Y values mean the joint is higher up and we can deduce which joint that is by height).
            var firstHalf = orderedBlobs.Take(orderedBlobs.Count() / 2).OrderByDescending(x => x.Value.Centroid.Y);
            var secondHalf =
                orderedBlobs.Skip(orderedBlobs.Count() / 2)
                    .Take(orderedBlobs.Count() / 2)
                    .OrderByDescending(x => x.Value.Centroid.Y);
            // Join collection back in to get each side from top down.
            var sorted = firstHalf.Union(secondHalf).Select(x => x.Value);
            return sorted.Select(x => new Blob(x)).ToList();
        }

        private static bool Between(int val, int low, int high)
        {
            return low < val && val < high;
        }
    }
}
