﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Kinect;
using Scandium.Enums;
using Scandium.EventArgs;
using Scandium.Interfaces;
using JointType = Scandium.Enums.JointType;
using System.Threading;
using System.Threading.Tasks;

namespace Scandium.Classes
{
    /// <summary>
    ///     A class responsible for capturing and processing the various sensors.
    /// </summary>
    internal class SensorDataHandler : ISensorDataHandler
    {
        /// <summary>
        ///     Constructor that initializes sensors.
        /// </summary>
        public SensorDataHandler(ICvInvoker scandiumCvInvoker)
        {
            // In minimum case we can use OpenCL for image processing.
            MessageBox = new MessageBox();
            PortFetcher = new PortFetcher();
            BlobDetector = new BlobDetector();
            _currentSessionState = SessionState.Standby;
            if (scandiumCvInvoker.HaveOpenCL)
                scandiumCvInvoker.UseOpenCL = true;
        }

        /// <summary>
        ///     Make sure Arduino Sensors are ready to be read from.
        /// </summary>
        public void InitArduinoSensors()
        {
            LoadCellPort = PortFetcher.GetLoadCellPort();
            if (LoadCellPort == null)
            {
                MessageBox.Show("Weight Sensor Error!",
                                "No weight sensors found. Weight data will not be recorded. Please check connections on unit.");
            }
        }

        /// <summary>
        ///     Initialize Kinect to capture Depth and Color frames only and initialize Calibration State. Set up event delegate.
        /// </summary>
        public void InitKinect()
        {
            ScandiumKinectSensor = new KinectSensor(Microsoft.Kinect.KinectSensor.GetDefault(),
                FrameSourceTypes.Depth | FrameSourceTypes.Infrared);
            ScandiumKinectSensor.MultiSourceFrameReader.MultiSourceFrameArrived += KinectFrameArrived;
        }

        /// <summary>
        ///     Make sure when we are done with the SensorDataHangler we do not keep using resources.
        /// </summary>
        public void Dispose()
        {
            CloseSensors();
            ScandiumKinectSensor?.MultiSourceFrameReader?.Dispose();
            LoadCellPort?.Close();
        }

        /// <summary>
        ///     Open all sensors and let data in.
        /// </summary>
        public void OpenSensors()
        {
            ScandiumKinectSensor.Open();
            LoadCellPort?.Open();
        }

        /// <summary>
        ///     Stop all data input to this class.
        /// </summary>
        public void CloseSensors()
        {
            ScandiumKinectSensor.Close();
            LoadCellPort?.Close();
        }

        /// <summary>
        ///     Ask for information from all sensors.
        /// </summary>
        /// <param name="ir">ColorFrame from Kinect.</param>
        /// <param name="depth">DepthFrame from Kinect.</param>
        public void QuerySensors(IInfraredFrame ir, IDepthFrame depth)
        {
            var loadCells = QueryLoadCells();
            var jointInfo = QueryJointInformation(ir, depth);
            if (jointInfo == null) return;
            var args = new SensorDataArrivedEventArgs(loadCells, jointInfo.Item2, jointInfo.Item1, DateTime.Now);
            SensorDataArrived?.Invoke(null, args);
        }

        /// <summary>
        ///     Ask for information from 4 load cells.
        /// </summary>
        /// <returns>Float array of arduino sensor data.</returns>
        public float[] QueryLoadCells()
        {
            var loadCell = new float[4];
            if (LoadCellPort != null)
            {
                try
                {
                    LoadCellPort.DiscardInBuffer();
                    LoadCellPort.Ping();
                    Thread.Sleep(10);
                    var indata = LoadCellPort.ReadLine();
                    // We read 5 data points from the arduino delimited by the | char
                    // 5 datapoints is for safety because sometimes the usb can cut out.
                    var parsedData = indata.Split('|');
                    if (parsedData.Length == 5)
                    {
                        for (var i = 0; i < 4; i++)
                        {
                            // TO-DO: This line might not be doing anything. Zack, look more into this please.
                            // if (parsedData[i + 1] == null) continue;
                            loadCell[i] = Single.Parse(parsedData[i + 1], CultureInfo.InvariantCulture.NumberFormat);
                            // offset the front load cells by 5
                            if (i == 2 || i == 3)
                            {
                                loadCell[i] -= 5.0F;
                            }
                        }
                    }
                }
                catch
                {
                    LoadCellPort.Close();
                    try
                    {
                        LoadCellPort.Open();
                    }
                    catch
                    {
                    }
                    loadCell[0] = 10.0f;
                    loadCell[1] = 10.0f;
                    loadCell[2] = 10.0f;
                    loadCell[3] = 10.0f;
                }
            }
            else
            {
                loadCell[0] = 10.0f;
                loadCell[1] = 10.0f;
                loadCell[2] = 10.0f;
                loadCell[3] = 10.0f;
            }
            return loadCell;
        }

        /// <summary>
        ///     Ask for Joint Dictionary and angles.
        /// </summary>
        /// <param name="ir">ColorFrame from Kinect.</param>
        /// <param name="depth">DepthFrame from Kinect.</param>
        /// <returns></returns>
        public Tuple<double[], Dictionary<JointType, Joint>> QueryJointInformation(IInfraredFrame ir, IDepthFrame depth)
        {
            var blobs = BlobDetector.FindBlobs(ir);
            var depthData = depth.GetDepthCoordinates(ScandiumKinectSensor);

            var i = 0;
            var joints = new Dictionary<JointType, Joint>();
            var angles = new double[4];
            // If we did not detect all joints, return empty tuple.
            if (blobs.Item1.Count != 6) return new Tuple<double[], Dictionary<JointType, Joint>>(angles, joints);
            foreach (var blob in blobs.Item1)
            {
                ushort sumOfPoints = 0;
                ushort validDepthCount = 0;
                // This part is a bit complex, basically we are getting the depth point from the blob X,Y
                ushort depthCoordinate;
                for (int j = Constants.PixelsOutMin; j <= Constants.PixelsOutMax; j += 1)
                {
                    depthCoordinate = depthData[((int) blob.Centroid.X + j) + ((int) blob.Centroid.Y + j)];
                    if (depthCoordinate >= Constants.MaximumDepthAllowed || depthCoordinate <= Constants.MinimumDepthAllowed)
                        continue;
                    sumOfPoints += depthCoordinate;
                    validDepthCount++;
                }

                if (validDepthCount > 0) // If any valid depth points are found
                {
                    depthCoordinate = (ushort)(sumOfPoints / validDepthCount); // Take average of any valid depth points found
                }
                else
                {
                    depthCoordinate = 0;
                }

                // Create joint, since we know the order we can use i for the joint type.
                if (depthCoordinate != 0)
                {
                    var testJoint = new Joint(blob.Centroid.X, blob.Centroid.Y, depthCoordinate, (JointType) i);
                    joints.Add(testJoint.JointType, testJoint);
                }
                else
                {
                    return null;
                }

                i++;
            }
            angles[0] = Joint.GetAngle(joints[JointType.LeftHip], joints[JointType.LeftKnee]);
            angles[1] = Joint.GetAngle(joints[JointType.LeftKnee], joints[JointType.LeftAnkle]);
            angles[2] = Joint.GetAngle(joints[JointType.RightHip], joints[JointType.RightKnee]);
            angles[3] = Joint.GetAngle(joints[JointType.RightKnee], joints[JointType.RightAnkle]);
            return new Tuple<double[], Dictionary<JointType, Joint>>(angles, joints);
        }

        /// <summary>
        ///     EventHandaler for Kinect Frame arrivals.
        /// </summary>
        /// <param name="sender">The object responsible for this event.</param>
        /// <param name="e">The Kinect MultiSourceFrame event args that contain all relevant frame.</param>
        public void KinectFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            // Acquire the unified frame.
            var reference = e.FrameReference.AcquireFrame();


            Task.Run(() =>
            {
                // Color and Depth mapping to get joint coordinates
                using (var infraredFrame = reference.InfraredFrameReference.AcquireFrame())
                using (var depthFrame = reference.DepthFrameReference.AcquireFrame())
                {
                    // If we do not have both frames do not go further.
                    if (infraredFrame == null || depthFrame == null) return;
                    var ir = new InfraredFrame(infraredFrame);
                    var depth = new DepthFrame(depthFrame);

                    // Kick off Calibration Event.
                    if (CurrentSessionState == SessionState.DebugOn)
                    {
                        var videoFrameArgs = new VideoFrameArrivedEventArgs(ir);
                        VideoFrameArrived?.Invoke(null, videoFrameArgs);
                    }
                    // Kick off Session.
                    if (CurrentSessionState == SessionState.SessionOn)
                        QuerySensors(ir, depth);
                }
            });
        }

        #region Members
        private SessionState _currentSessionState;
        public IKinectSensor ScandiumKinectSensor { get; set; }
        public IMessageBox MessageBox { get; set; }
        public IPortFetcher PortFetcher { get; set; }
        public SessionState CurrentSessionState
        {
            get {return _currentSessionState;}
            set
            {
                // Only close or open sensors when state is actually being changed
                if (_currentSessionState == SessionState.Standby && value != SessionState.Standby)
                {
                    OpenSensors();
                }
                else if (_currentSessionState != SessionState.Standby && value == SessionState.Standby)
                {
                    CloseSensors();
                }
                _currentSessionState = value;
            }
        }
        public event EventHandler<VideoFrameArrivedEventArgs> VideoFrameArrived;
        public event EventHandler<SensorDataArrivedEventArgs> SensorDataArrived;
        public ISerialPort LoadCellPort { get; set; }
        public IBlobDetector BlobDetector { get; set; }
        #endregion
    }

    internal delegate void VideoFrameArrivedEventDelegate(VideoFrameArrivedEventArgs args);
    internal delegate void SensorDataArrivedEventDelegate(SensorDataArrivedEventArgs args);
}