﻿using System.Windows;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class MessageBox : IMessageBox
    {
        public MessageBoxResult Show(string title, string message)
        {
            return System.Windows.MessageBox.Show(message, title);
        }
    }
}