﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scandium.EventArgs;

namespace Scandium.Classes
{
    internal class Session
    {
        public Session()
        {
            Id = Guid.NewGuid().ToString();
            Datapoints = new List<Datapoint>();
        }
        public string Id;
        public string PatientId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<Datapoint> Datapoints { get; set; }
    }

    internal class Datapoint
    {
        public Datapoint()
        {
            Id = Guid.NewGuid().ToString();
        }

        public Datapoint(SensorDataArrivedEventArgs e)
        {
            Id = Guid.NewGuid().ToString();
            TimeOfRecording = e.TimeOfEvent;
            LeftHipKneeAngle = e.Angles[0];
            LeftKneeAnkleAngle = e.Angles[1];
            RightHipKneeAngle = e.Angles[2];
            RightKneeAnkleAngle = e.Angles[3];
            FrontLeftWeight = e.LoadCells[0];
            FrontRightWeight = e.LoadCells[1];
            BackLeftWeight = e.LoadCells[2];
            BackRightWeight = e.LoadCells[3];
        }

        public string Id;
        public DateTime TimeOfRecording { get; set; }
        public double LeftHipKneeAngle { get; set; }
        public double RightHipKneeAngle { get; set; }
        public double LeftKneeAnkleAngle { get; set; }
        public double RightKneeAnkleAngle { get; set; }
        public double FrontLeftWeight { get; set; }
        public double FrontRightWeight { get; set; }
        public double BackLeftWeight { get; set; }
        public double BackRightWeight { get; set; }
    }
}
