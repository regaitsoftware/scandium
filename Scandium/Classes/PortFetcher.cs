﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class PortFetcher : IPortFetcher
    {
        public string[] GetPortNames()
        {
            return System.IO.Ports.SerialPort.GetPortNames();
        }

        public List<ISerialPort> GetAvailablePorts()
        {
            var availablePorts = new List<ISerialPort>();
            var portNames = GetPortNames();
            if (portNames.Length == 0)
                return availablePorts;

            foreach (var name in portNames)
            {
                Console.WriteLine("Looking for data sensors on: " + name);
                var port = new SerialPort(name);
                try
                {
                    port.Open();
                    availablePorts.Add(port);
                }
                catch (IOException)
                {
                    break;
                }
            }
            return availablePorts;
        }

        public ISerialPort GetLoadCellPort()
        {
            var loadCellPorts = GetAvailablePorts();
            Console.WriteLine(@"ERROR: Other Com Ports Found");
            // There are serial ports that are found on the machine, iterate over them and test to see if any are the load cells.
            for (int i = 0; i < loadCellPorts.Count; i++)
            {
                Thread.Sleep(10);
                var port = loadCellPorts[i];
                port.Ping();
                Thread.Sleep(10);
                var bufferData = port.ReadLine();
                string testData = null;
                if (bufferData != "")
                {
                    port.Ping();
                    testData = port.ReadLine();
                }
                var parsedData = testData?.Split('|');
                if (parsedData?.Length == 5)
                {
                    Console.WriteLine("Found Data Sensors on: " + port.Name);
                    port.Close();
                    return port;
                }
                port.Close();
                loadCellPorts.Remove(port);
            }
            return null;
        }
    }
}
