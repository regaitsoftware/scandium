﻿using Emgu.CV;
using Emgu.CV.Structure;

namespace Scandium.Classes
{
    static class Constants
    {
        public static ScalarArray LowerScalarArray = new ScalarArray(new Hsv(0, 0, 247).MCvScalar);
        public static ScalarArray UpperScalarArray = new ScalarArray(new Hsv(256, 256, 256).MCvScalar);
        public const int MinimumBlobSize = 280;
        public const int MaximumBlobSize = 800;
        public const int MinimumBlobXPosition = 73;
        public const int MaximumBlobXPosition = 500;
        public const int MinimumDepthAllowed = 300;
        public const int MaximumDepthAllowed = 1300;
        public const int PixelsOutMin = 5;
        public const int PixelsOutMax = PixelsOutMin + 4;
    }
}
