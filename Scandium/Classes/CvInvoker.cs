﻿using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class CvInvoker : ICvInvoker
    {
        public bool HaveOpenCL => Emgu.CV.CvInvoke.HaveOpenCL;
        public bool UseOpenCL
        {
            get { return Emgu.CV.CvInvoke.UseOpenCL; }
            set { Emgu.CV.CvInvoke.UseOpenCL = value; }
        }
    }
}
