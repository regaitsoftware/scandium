using System;
using System.Collections.Generic;
using System.Windows.Media;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.PointMarkers;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class ChartPlotter : IChartPlotter
    {
        public ChartPlotter(Microsoft.Research.DynamicDataDisplay.ChartPlotter plotter)
        {
            Plotter = plotter;
            Pen = new Pen(Brushes.Blue, 2.0);
            CirclePointMarker = new CirclePointMarker {Size = 0.0, Fill = Brushes.Red};
            PenDescription = new PenDescription("Variation");
        }

        public Pen Pen { get; set; }
        public CirclePointMarker CirclePointMarker { get; set; }
        public PenDescription PenDescription { get; set; }
        public Microsoft.Research.DynamicDataDisplay.ChartPlotter Plotter { get; set; }
        public IEnumerable<double> DataSourceX { get; set; }
        public IEnumerable<double> DataSourceY { get; set; }

        public void Clear()
        {
            Plotter.Children.RemoveAll(typeof(LineGraph));
        }

        public void CreateLineGraph(IEnumerable<double> dataSourceX, IEnumerable<double> dataSourceY)
        {
            DataSourceX = dataSourceX;
            DataSourceY = dataSourceY;
            var xData = new EnumerableDataSource<double>(DataSourceX);
            xData.SetXMapping(x => x);
            var yData = new EnumerableDataSource<double>(DataSourceY);
            yData.SetYMapping(y => y);
            var dataSource = new CompositeDataSource(xData, yData);
            Clear();
            Plotter.AddLineGraph(dataSource, Pen, CirclePointMarker, PenDescription);
            Plotter.FitToView();
        }

        public void CreateLineGraph(Tuple<IEnumerable<double>, IEnumerable<double>> tuple)
        {
            CreateLineGraph(tuple.Item1, tuple.Item2);
        }

        public void CreateLineGraph()
        {
            CreateLineGraph(DataSourceX, DataSourceY);
        }
    }
}