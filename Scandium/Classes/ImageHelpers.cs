﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Size = System.Drawing.Size;

namespace Scandium.Classes
{
    /// <summary>
    ///     General image processing helper class. Includes converting Kinect Frames to WPF friendly data types, and blob
    ///     detection.
    /// </summary>
    internal static class ImageHelpers
    {
        /// <summary>
        ///     This method ensures we do not hold onto original versions of converted datatypes.
        ///     Uses C version of DeletObject.
        /// </summary>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        public static BitmapSource ToBitmapSource(this IImage image)
        {
            using (var source = image.Bitmap)
            {
                var ptr = source.GetHbitmap(); //obtain the Hbitmap

                var bs = Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }

        /// <summary>
        ///     Given a BitmapSource convert to Bitmap.
        /// </summary>
        /// <param name="bSource">BitmapSource to convert to bitmap.</param>
        /// <returns>Bitmap for EMGU CV conversion</returns>
        public static Bitmap ToBitmap(this BitmapSource bSource)
        {
            Bitmap bmp;
            using (var ms = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bSource));
                enc.Save(ms);
                bmp = new Bitmap(ms);
            }
            return bmp;
        }

        #region blob detection helpers

        /// <summary>
        ///     Perform a Gaussian Blur on the image to make color detection less computationally expensive/
        /// </summary>
        /// <param name="inputImage">The EMGU CV UMat to smooth</param>
        public static void SmoothImage(this UMat inputImage)
        {
            using (var umat = new UMat())
            {
                CvInvoke.GaussianBlur(inputImage, umat, new Size(29, 29), 2.0, 2.0);
                umat.CopyTo(inputImage);
            }
        }

        #endregion
    }
}