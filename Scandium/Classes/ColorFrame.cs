﻿using System.Windows.Media;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.Kinect;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class ColorFrame : IColorFrame
    {
        public int Width => _colorFrame.FrameDescription.Width;
        public int Height => _colorFrame.FrameDescription.Height;
        public ColorImageFormat RawColorImageFormat => _colorFrame.RawColorImageFormat;
        private readonly Microsoft.Kinect.ColorFrame _colorFrame;

        public ColorFrame(Microsoft.Kinect.ColorFrame colorFrame)
        {
            _colorFrame = colorFrame;
        }

        /// <summary>
        ///     Converts a ColorFrame to an EMGU CV compatible Image.
        /// </summary>
        /// <returns>A EMGU CV image for processing.</returns>
        public Image<Bgr, byte> ToCvImage()
        {
            return new Image<Bgr, byte>(ToBitmapSource().ToBitmap());
        }

        /// <summary>
        ///     Converts a ColorFrame to an EMGU CV compatible UMat. UMats are more reliably processed on GPU instead of CPU.
        /// </summary>
        /// <returns>A EMGU CV UMat for processing.</returns>
        public UMat ToUMat()
        {
            return ToCvImage().ToUMat();
        }

        /// <summary>
        ///     Converts a ColorFrame to a BitmapSource for displaying on WPF forms.
        /// </summary>
        /// <returns>A BitmapSource Compatible with WPF ImageViews.</returns>
        public BitmapSource ToBitmapSource()
        {
            // Get formatting information from frame.
            var format = PixelFormats.Bgr32;

            var pixels = new byte[Width * Height * ((format.BitsPerPixel + 7) / 8)];

            // Make sure frame is in correct ColorImageFormat
            if (RawColorImageFormat == ColorImageFormat.Bgra)
            {
                CopyRawFrameDataToArray(pixels);
            }
            // Otherwise make the ColorImageFormat Bgra
            else
            {
                CopyConvertedFrameDataToArray(pixels, ColorImageFormat.Bgra);
            }

            var stride = Width * format.BitsPerPixel / 8;
            return BitmapSource.Create(Width, Height, 96, 96, format, null, pixels, stride);
        }

        public void CopyConvertedFrameDataToArray(byte[] frameData, ColorImageFormat colorFormat)
        {
            _colorFrame.CopyConvertedFrameDataToArray(frameData, colorFormat);
        }

        public void CopyRawFrameDataToArray(byte[] frameData)
        {
            _colorFrame.CopyRawFrameDataToArray(frameData);
        }

        public void Dispose()
        {
            _colorFrame.Dispose();
        }
    }
}
