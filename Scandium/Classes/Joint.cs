﻿using System;
using Scandium.Enums;

namespace Scandium.Classes
{
    /// <summary>
    ///     A class representing the 3D position of the joint and it's type.
    /// </summary>
    internal class Joint
    {
        public JointType JointType;
        public double X;
        public double Y;
        public double Z;

        /// <summary>
        ///     Joint constructor taking in 3 doubles for position, and type.
        /// </summary>
        public Joint(double x, double y, double z, JointType jointType)
        {
            X = x;
            Y = y;
            Z = z;
            JointType = jointType;
        }

        public override string ToString()
        {
            return $"({X},{Y},{Z}), {JointType}";
        }

        /// <summary>
        ///     Gets the angle between joints. Specifically for Hip-Knee angle
        /// </summary>
        /// <returns>Angle between two joints.</returns>
        public static double GetAngle(Joint x, Joint y)
        {
            var theta = 0.0;
            
            // Height of joints is almost the same.
            if (Math.Abs(x.Y - y.Y) < 0.1f)
            {
                theta = 45;
            }
            else
            {
                theta = 180/Math.PI*Math.Acos((x.Y - y.Y)/
                                                Math.Sqrt(Math.Pow(x.X - y.X, 2) +
                                                        Math.Pow(x.Y - y.Y, 2) +
                                                        Math.Pow(x.Z - y.Z, 2)));
            }
            // Hip is closer to the camera (infront of the knee).
            if (x.Z < y.Z)
            {
                theta = -theta;
            }
            
            return theta;
        }

        public double DotProduct(Joint joint)
        {
            return X*joint.X + Y*joint.Y + Z*joint.Z;
        }
    }
}