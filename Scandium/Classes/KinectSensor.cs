﻿using Microsoft.Kinect;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class KinectSensor : IKinectSensor
    {
        private readonly Microsoft.Kinect.KinectSensor _sensor;

        public KinectSensor(Microsoft.Kinect.KinectSensor sensor, FrameSourceTypes frameSourceTypes)
        {
            _sensor = sensor;
            CoordinateMapper = _sensor.CoordinateMapper;
            MultiSourceFrameReader = OpenMultiSourceFrameReader(frameSourceTypes);
        }

        public CoordinateMapper CoordinateMapper { get; }
        public MultiSourceFrameReader MultiSourceFrameReader { get; }

        public MultiSourceFrameReader OpenMultiSourceFrameReader(FrameSourceTypes frameSourceTypes)
        {
            return _sensor.OpenMultiSourceFrameReader(frameSourceTypes);
        }

        public void Close()
        {
            _sensor.Close();
        }

        public void Open()
        {
            _sensor.Open();
        }
    }
}