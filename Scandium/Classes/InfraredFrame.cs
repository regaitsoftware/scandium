﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class InfraredFrame : IInfraredFrame
    {
        private readonly Microsoft.Kinect.InfraredFrame _irFrame;

        public InfraredFrame(Microsoft.Kinect.InfraredFrame irFrame)
        {
            _irFrame = irFrame;
        }

        public void Dispose()
        {

            _irFrame.Dispose();
        }

        public int Width => _irFrame.FrameDescription.Width;
        public int Height => _irFrame.FrameDescription.Height;

        public Image<Bgr, byte> ToCvImage()
        {
            return new Image<Bgr, byte>(ToBitmapSource().ToBitmap());
        }

        public UMat ToUMat()
        {
            return ToCvImage().ToUMat();
        }

        /// <summary>
        ///     Converts an InfraredFrame to a BitmapSource for displaying on WPF forms.
        /// </summary>
        /// <returns>A BitmapSource Compatible with WPF ImageViews.</returns>
        public BitmapSource ToBitmapSource()
        {
            // Get formatting information from frame.
            var format = PixelFormats.Bgr32;

            var frameData = new ushort[Width * Height];
            var pixels = new byte[Width * Height * (format.BitsPerPixel + 7) / 8];
            CopyFrameDataToArray(frameData);

            var colorIndex = 0;
            // Convert Infrared pixels to color pixels.
            foreach (var ir in frameData)
            {
                var intensity = (byte)(ir >> 7);

                pixels[colorIndex++] = (byte)(intensity / 1); // Blue
                pixels[colorIndex++] = (byte)(intensity / 1); // Green   
                pixels[colorIndex++] = (byte)(intensity / 0.4); // Red

                colorIndex++;
            }

            var stride = Width * format.BitsPerPixel / 8;

            return BitmapSource.Create(Width, Height, 96, 96, format, null, pixels, stride);
        }

        public void CopyFrameDataToArray(ushort[] frameData)
        {
            _irFrame.CopyFrameDataToArray(frameData);
        }
    }
}