﻿using System.Windows.Media;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Microsoft.Kinect;
using Scandium.Interfaces;

namespace Scandium.Classes
{
    internal class DepthFrame : IDepthFrame
    {
        private readonly Microsoft.Kinect.DepthFrame _depthFrame;
        public int Width => _depthFrame.FrameDescription.Width;
        public int Height => _depthFrame.FrameDescription.Height;
        public ushort DepthMinReliableDistance => _depthFrame.DepthMinReliableDistance;
        public ushort DepthMaxReliableDistance => _depthFrame.DepthMaxReliableDistance;
        
        public DepthFrame(Microsoft.Kinect.DepthFrame depthFrame)
        {
            _depthFrame = depthFrame;
        }

        /// <summary>
        ///     Converts a ColorFrame to an EMGU CV compatible Image.
        /// </summary>
        /// <returns>A EMGU CV image for processing.</returns>
        public Image<Bgr, byte> ToCvImage()
        {
            return new Image<Bgr, byte>(ToBitmapSource().ToBitmap());
        }

        /// <summary>
        ///     Converts a ColorFrame to an EMGU CV compatible UMat. UMats are more reliably processed on GPU instead of CPU.
        /// </summary>
        /// <returns>A EMGU CV UMat for processing.</returns>
        public UMat ToUMat()
        {
            return ToCvImage().ToUMat();
        }

        /// <summary>
        ///     Converts a DepthFrame to a BitmapSource for displaying on WPF forms.
        /// </summary>
        /// <returns>A BitmapSource Compatible with WPF ImageViews.</returns>
        public BitmapSource ToBitmapSource()
        {
            // Get formatting information from frame.
            var format = PixelFormats.Bgr32;
            var minDepth = DepthMinReliableDistance;
            var maxDepth = DepthMaxReliableDistance;

            var pixelData = new ushort[Width * Height];
            var pixels = new byte[Width * Height * (format.BitsPerPixel + 7) / 8];
            _depthFrame.CopyFrameDataToArray(pixelData);

            var colorIndex = 0;
            // Convert Depth pixels to color pixels.
            foreach (var depth in pixelData)
            {
                var intensity = (byte)(depth >= minDepth && depth <= maxDepth ? depth : 0);

                pixels[colorIndex++] = intensity; // Blue
                pixels[colorIndex++] = intensity; // Green
                pixels[colorIndex++] = intensity; // Red

                ++colorIndex;
            }

            var stride = Width * format.BitsPerPixel / 8;

            return BitmapSource.Create(Width, Height, 96, 96, format, null, pixels, stride);
        }


        public void CopyFrameDataToArray(ushort[] frameData)
        {
            _depthFrame.CopyFrameDataToArray(frameData);
        }

        /// <summary>
        ///     Given a DepthFrame and a ScandiumKinectSensor return the Depth Coordinates.
        /// </summary>
        /// <param name="scandiumKinectSensor">ScandiumKinectSensor used to do coordinate mappoing from DepthFrame.</param>
        /// <returns></returns>
        public ushort[] GetDepthCoordinates(IKinectSensor scandiumKinectSensor)
        {
            // Array should contain ColorFrame resolution product amount of pixel space.
            var depthSpacePoints = new DepthSpacePoint[1920 * 1080];
            // Result should hold DepthFrame resolution product amount of pixel space.
            var result = new ushort[512 * 424];
            CopyFrameDataToArray(result);
            scandiumKinectSensor.CoordinateMapper.MapColorFrameToDepthSpace(result, depthSpacePoints);
            return result;
        }

        public void Dispose()
        {
            _depthFrame.Dispose();
        }
    }
}