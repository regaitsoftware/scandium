﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scandium.Enums
{
    /// <summary>
    ///     Enum to identify the type of joint detected.
    /// </summary>
    internal enum JointType
    {
        LeftHip,
        LeftKnee,
        LeftAnkle,
        RightHip,
        RightKnee,
        RightAnkle
    }
}
