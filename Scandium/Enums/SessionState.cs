namespace Scandium.Enums
{
    internal enum SessionState
    {
        Standby,
        SessionOn,
        DebugOn
    }
}