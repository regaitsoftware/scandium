﻿using System.Windows;
using Scandium.Classes;
using Scandium.Windows;

namespace Scandium
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var loginView = new LoginView();
            loginView.Show();

            //var patientView = new Windows.PatientView(new SensorDataHandler(new CvInvoker()));
            //patientView.Show();
        }
    }
}