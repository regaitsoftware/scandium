﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Castle.Core.Internal;
using Newtonsoft.Json;
using Scandium.Classes;

namespace Scandium.Windows
{
    struct LoginObject
    {
        public string UserName;
        public string Password;
    }

    struct LoginResult
    {
        [JsonProperty("access_token")]
        public string AccessToken;
        [JsonProperty("id")]
        public string Id;
        [JsonProperty("userName")]
        public string UserName;
        [JsonProperty("error")]
        public string Error;
        [JsonProperty("error_descriptor")]
        public string ErrorDescriptor;
    }

    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView
    {
        private const int kMinPinLength = 6;
        private string pinString = "";
        private IEnumerable<Button> buttons;
        internal Classes.MessageBox errorMessageBox { get; set; }

        public LoginView()
        {
            InitializeComponent();
            errorMessageBox = new Classes.MessageBox();
            buttons = NumpadGrid.Children.OfType<Button>();
        }

        private async void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(pinString) || pinString.Length < kMinPinLength)
            {
                errorMessageBox.Show("Bad Pin", "Please enter a valid pin");
                pinString = string.Empty;
                pinBox.Content = string.Empty;
                return;
            }

            string url = @"https://localhost:44306/Token";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";

            try
            {
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var loginObj = new LoginObject
                    {
                        // Hard code this for now, eventually we will need to have some sort of setup that assignes emails to walkers
                        UserName = "testpatient@example.com",
                        Password = pinString,
                    };
                    var urlParams = $"grant_type=password&password={loginObj.Password}&username={loginObj.UserName}";
                    streamWriter.Write(urlParams);
                }
            }
            catch (WebException)
            {
                ResetForm();
                loginFail.Visibility = Visibility.Visible;
                loginFail.Content = "Could not connect to server. Please try again later.";
                return;
            }

            LoginResult response;
            try
            {
                nameLabel.Content = "Logging in...";
                buttons.ForEach(button => button.IsEnabled = false);
                var httpResponse = await httpWebRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    response = JsonConvert.DeserializeObject<LoginResult>(result);
                }
            }
            catch (WebException)
            {
                loginFail.Content = "Could not connect to server, please try again later.";
                response = new LoginResult();
            }

            if (VerifyLogin(response))
            {
                var patientView = new PatientView(new SensorDataHandler(new CvInvoker()))
                {
                    UserId = response.Id,
                    AccessToken = response.AccessToken
                };
                Close();
                patientView.Show();
            }
            else
            {
                ResetForm();
                loginFail.Visibility = Visibility.Visible;
                loginFail.Content = response.ErrorDescriptor;
            }

            buttons.ForEach(button => button.IsEnabled = true);
            enterButton.IsEnabled = false;
        }

        private void ResetForm()
        {
            pinString = "";
            pinBox.Content = "";
            nameLabel.Content = "Login";
            buttons.ForEach(button => button.IsEnabled = true);
        }

        private bool VerifyLogin(LoginResult response)
        {
            return response.Id != string.Empty &&
                   response.AccessToken != null && response.Error == null;
        }

        private void numberButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;

            pinString += button.Content;

            pinBox.Content += "•";

            enterButton.IsEnabled = pinBox.Content.ToString().Length >= kMinPinLength;

            if (pinBox.Content.ToString().Length == kMinPinLength)
            {
                // This makes sure the UI thread finishes before kicking off login event.
                Dispatcher.Invoke(new Action(() => { }), DispatcherPriority.ContextIdle, null);
                loginButton_Click(new object(), new RoutedEventArgs());
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (pinString.Length > 0)
            {
                pinString = pinString.Substring(0, pinString.Length - 1);
                pinBox.Content = ((string)pinBox.Content).Substring(0, pinString.Length);
            }
        }
    }
}