﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;
using Scandium.Classes;
using Scandium.Enums;
using Scandium.EventArgs;
using Scandium.Interfaces;

namespace Scandium.Windows
{
    /// <summary>
    ///     The WPF window the user will see on the walker.
    /// </summary>
    public partial class PatientView : Window
    {

        #region Members
        private readonly ISensorDataHandler _sensorDataHandler;
        public string UserId { get; set; }
        public string AccessToken { get; set; }
        private Session _session = new Session();
        #endregion

        internal PatientView(ISensorDataHandler sensorDataHandler)
        {
            InitializeComponent();
            _sensorDataHandler = sensorDataHandler;
            _sensorDataHandler.VideoFrameArrived += GetDebugVideoFrame;
            //_sensorDataHandler.InitArduinoSensors();
            _sensorDataHandler.InitKinect();
        }

        #region Event handlers
        /// <summary>
        /// Event handler that runs when the window is closed.
        /// </summary>
        private void OnWindowClosed(object sender, System.EventArgs e)
        {
            _sensorDataHandler.Dispose();
        }

        /// <summary>
        ///     Event Handler for Calibration video feed. Do so only every other frame for best perforance.
        /// </summary>
        internal void GetDebugVideoFrame(object sender, VideoFrameArrivedEventArgs e)
        {
            this.Dispatcher.Invoke((Action) (() =>
            {
                var res = _sensorDataHandler.BlobDetector.FindBlobs(e.VideoFrame);
                DebugCamera.Source = res.Item2.ToBitmapSource();
            }));
        }

        /// <summary>
        ///     EventHandler for sensor data arriving.
        /// </summary>
        private void GetSensorData(object sender, SensorDataArrivedEventArgs e)
        {
            RecordData(e);
            UpdateLoadCellCanvases(e);
        }

        /// <summary>
        ///     Write to local files the information from the file. Eventually this might need to be a local db so we can perform
        ///     post processing to remove outliers.
        /// </summary>
        private void RecordData(SensorDataArrivedEventArgs e)
        {
            if (e.Joints.Count < 6) return;
            _session.Datapoints.Add(new Datapoint(e));
        }

        /// <summary>
        ///     Change the load cell indicating canvases to reflect weight distribution.
        /// </summary>
        private void UpdateLoadCellCanvases(SensorDataArrivedEventArgs e)
        {
            var totalLoadCell = e.LoadCells.Sum();
            var value = e.LoadCells[0] / totalLoadCell;
            var loadCellColor0 = new Color
            {
                A = 255,
                R = (byte)(value * 255),
                G = (byte)(255 - (value + 15 * 255) / 2),
                B = (byte)value
            };

            //Lower Left
            value = e.LoadCells[1] / totalLoadCell;
            var loadCellColor1 = new Color
            {
                A = 255,
                R = (byte)(value * 255),
                G = (byte)(255 - (value + 15 * 255) / 2),
                B = (byte)value
            };

            //Upper Right
            value = e.LoadCells[2] / totalLoadCell;
            var loadCellColor2 = new Color
            {
                A = 255,
                R = (byte)(value * 255),
                G = (byte)(255 - (value + 15 * 255) / 2),
                B = (byte)value
            };

            //Upper Left
            value = e.LoadCells[3] / totalLoadCell;
            var loadCellColor3 = new Color
            {
                A = 255,
                R = (byte)(value * 255),
                G = (byte)(255 - (value + 15 * 255) / 2),
                B = (byte)value
            };

            Dispatcher.Invoke(() =>
            {
                LowerRightLoadCellStatus.Background = new SolidColorBrush(loadCellColor0);
                LowerLeftLoadCellStatus.Background = new SolidColorBrush(loadCellColor1);
                UpperRightLoadCellStatus.Background = new SolidColorBrush(loadCellColor2);
                UpperLeftLoadCellStatus.Background = new SolidColorBrush(loadCellColor3);
            });
        }

        /// <summary>
        ///     Change WPF to reflect state of session while calling for the sensors to open or close.
        /// </summary>
        public async void OnStartSessionButtonClicked(object sender, RoutedEventArgs e)
        {
            if (_sensorDataHandler.CurrentSessionState == SessionState.Standby)
            {
                _session = new Session { StartTime = DateTime.Now, PatientId = UserId };
                SessionButton.Content = "Stop Session";
                LoadCellCanvases.Visibility = Visibility.Visible;
                _sensorDataHandler.CurrentSessionState = SessionState.SessionOn;
                _sensorDataHandler.SensorDataArrived += GetSensorData;
            }
            else
            {
                var task = Task.Factory.StartNew(PushSessions);
                SessionButton.Content = "Start Session";
                LoadCellCanvases.Visibility = Visibility.Hidden;
                NetworkErrorMessage.Visibility = Visibility.Hidden;
                _sensorDataHandler.CurrentSessionState = SessionState.Standby;
                _sensorDataHandler.SensorDataArrived -= GetSensorData;
                try
                {
                    await task;
                }
                catch (WebException)
                {
                    NetworkErrorMessage.Content = "Network error uploading session data";
                    NetworkErrorMessage.Visibility = Visibility.Visible;
                }
            }
        }

        private void PushSessions()
        {
            if (_session.Datapoints.Count <= 0) return;
            _session.EndTime = DateTime.Now;
            // Post to site
            const string url = @"https://localhost:44306/api/session/";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers["bearer"] = AccessToken;
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(_session);
                streamWriter.Write(json);
            }

            var httpResponse = httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                JsonConvert.DeserializeObject<Session>(result);
            }
        }

        /// <summary>
        ///     Change WPF to reflect calibration state while calling for the sensors to open or close.
        /// </summary>
        public void OnCalibrateButtonClicked(object sender, RoutedEventArgs e)
        {
            if (_sensorDataHandler.CurrentSessionState == SessionState.Standby)
            {
                DebugButton.Content = "Stop Debugging";
                _sensorDataHandler.CurrentSessionState = SessionState.DebugOn;
                DebugCamera.Visibility = Visibility.Visible;
            }
            else
            {
                DebugButton.Content = "Debug";
                _sensorDataHandler.CurrentSessionState = SessionState.Standby;
                DebugCamera.Visibility = Visibility.Hidden;
            }
        }
        #endregion
    }
}