
//Load Cell Variables
int analogPin0 = 0;    // potentiometer wiper (middle terminal) connected to analog pin 3
int analogPin1 = 1;     // rear left wheel
int analogPin2 = 2;    // Front right wheel
int analogPin3 = 3;    // Front left wheel
int sleepPin=10;

             // outside leads to ground and +5V

float val_0 = 0;       // variable to store the value read
float val_1 = 0;
float val_2 = 0;
float val_3 = 0;
float analog_0 = 0;
float analog_1 = 0;
float analog_2 = 0;
float analog_3 = 0;



//Encoder Variables
int A = 11;  //Blue
int B = 12;  //White
int C = 13;  //Yellow
int Time_A;
int Time_B;
int Time_C;
int val_A;
int val_B;
int val_C;
int RPM = 0;
float Rotation;



void setup()

{

  Serial.begin(9600);   // Setup serial
    pinMode(A, INPUT);    // Setup pins for hall effect sensors
  pinMode(B, INPUT);
  pinMode(C, OUTPUT);
  digitalWrite(C,LOW);
}



void loop()

{

  //Load Cell Loop

  analog_0 = analogRead(analogPin0);   // read the input pin
  analog_1 = analogRead(analogPin1);
  analog_2 = analogRead(analogPin2);
  analog_3 = analogRead(analogPin3);

  /*
   * 
   * ------------------------------------------------------------------
   * The Following are the vaules that show the weight of the load cell
   * ------------------------------------------------------------------
   * 
   */

  val_0 = -0.0000001*analog_0*analog_0+0.096*analog_0-1.9385;     // Amplifies the read value for testing purposes. Eventually this will be the calibatrion 
  val_1 = 0.00001*analog_1*analog_1 + 0.0827*analog_1-2.8702;
  val_2 = -0.00002*analog_2*analog_2+0.1144*analog_2-2.079;
  val_3 = -0.00001*analog_3*analog_3+0.102*analog_3+0.5709;

    if( val_0< 0)       // Zero's all load cells so only reads if pressure is applied to load cell
    {
      val_0=0;
    }
    if( val_1< 0)
    {
      val_1=0;
    }
    if( val_2< 0)
    {
      val_2=0;
    }


  //Encoder Loop
  digitalWrite(sleepPin, HIGH);
  
//  val_A = digitalRead(A);   //set variable to value from pin read out 
 // val_B = digitalRead(B);
  //val_C = digitalRead(C);
  
    if (val_A<1)        //if hall effect sensor is triggered the time stamp for that sensor is instantiated 
    {
    Time_A = millis();
    }
    if (val_B<1)
    {
    Time_B = millis();
    }
   if (val_C<1)
    {
    Time_C = millis();
    }

  if ((Time_A < Time_B && Time_A < Time_C && Time_B < Time_C))  //Compare time stamps to only record forward motion value
  {
    RPM++;
    Time_A = 0;
    Time_B = 0;
    Time_C = 0;
  }
  

  //Serial.print("RPM: ");
  //Serial.println(RPM);      //RPM: Amount of rotation
  Rotation=RPM*3.14*0.125;  //Rotation: Distance
  if (Serial.available() > 0) {
    Serial.read();
    digitalWrite(C,HIGH);
    Serial.print("|");
    Serial.print(val_0);
    Serial.print("|");
    Serial.print(val_1);
    Serial.print("|");
    Serial.print(val_2);
    Serial.print("|");
    Serial.println(val_3);
  }
  else{
    digitalWrite(C,LOW);
  }
  //Load cell values are val_0, val_1, val_2, val_3
  //delay(15);
  
}
